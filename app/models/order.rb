class Order < ApplicationRecord
  has_many :order_items
  has_many :menu_items, through: :order_items
  accepts_nested_attributes_for :order_items, allow_destroy: true, reject_if: :all_blank

  after_save :calculate_total

  def calculate_total

    subtotal = menu_items.sum(:price)
    tax = subtotal * 0.1
    update_columns(subtotal: subtotal, tax: tax)

  end

  def amount_due
    subtotal + tax
  end
end
