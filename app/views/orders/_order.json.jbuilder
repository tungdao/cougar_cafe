json.extract! order, :id, :name, :subtotal, :tax, :amount_due, :created_at, :updated_at
json.url order_url(order, format: :json)
